# Image Loader Client

A dead-simple API client for Image Loader, developed for **EUWebChallenge**.

## Installation

Add this line to your application's Gemfile:

    gem 'image_loader_client'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install image_loader_client

## Usage

By default ImageLoaderClient expects image loader server to be launched at *http://localhost:5000*, but you can
change this url before making request(s):

    ImageLoaderClient.image_loader_url = 'http://new-url.url'
    # all further requests from ImageLoaderClient will be addressed to http://new-url.url

You can use ImageLoaderClient to requests concerning the jobs and images of the server:

    ImageLoaderClient.jobs.get # fetch all jobs
    ImageLoaderClient.jobs.get(1) # fetch a job with id 1
    ImageLoaderClient.jobs.post(url: 'unsplash.com') # create a job
    ImageLoaderClient.jobs.delete(1) # delete a job with id 1
    ImageLoaderClient.jobs(1).images.get # fetch all images from job with id 1
    ImageLoaderClient.jobs(1).images.get(2) # fetch an image with id 2 from job with id 1

All responses can be processed either through blocks (yielding status and body), or inline
(returning an instance of `Faraday::Request`), e.g.:

 - through a block:

        ImageLoaderClient.jobs.get |status, body|
            if status == 200
                # do something with body, which in this case is an array of hashes that represent jobs
            else
                # handle error
            end
        end
  - inline:

         response = ImageLoaderClient.jobs.get #<Faraday::Response:0x0000000....>
         response.status #200
         response.body #in this case body an array of hashes that represent jobs

## Errors
When ImageLoaderClient cannot connect to the loader server, it throws an `ImageLoaderClient::ConnectionError`, which
 you can handle, for example, displaying a warning message for a user.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

