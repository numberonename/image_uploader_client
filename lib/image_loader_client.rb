require 'active_support/all'
require 'faraday'
require 'faraday_middleware'
require 'net/http'

project_root = File.expand_path('..', File.dirname(__FILE__))

Dir["#{project_root}/lib/image_loader_client/**/*.rb"].each {|file| require file }

module ImageLoaderClient
  API_PREFIX = 'api'

  class ConnectionError < StandardError; end
  class ClientError < StandardError; end
  class BannedMethodError < StandardError; end

  class << self

    ApiManager::ROOT_RESOURCES.each do |resource|
      define_method resource do |id = nil|
        api_manager.send(resource, id)
      end
    end

    def image_loader_url
      @image_loader_url ||= 'http://localhost:5000/'
    end

    def image_loader_url=(url)
      @image_loader_url = url
      @api_manager = ApiManager.new(url)
    end

    def api_manager
      @api_manager ||= ApiManager.new(image_loader_url)
    end
  end
end
