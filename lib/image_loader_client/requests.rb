module ImageLoaderClient
  module Requests
    def get(opts = {}, &block)
      opts = { id: opts } unless opts.is_a? Hash
      request(:get, opts, &block)
    end

    def post(opts = {}, &block)
      request(:post, opts, &block)
    end

    def delete(opts = {}, &block)
      opts = { id: opts } unless opts.is_a? Hash
      request(:delete, opts, &block)
    end

    def path_to_resource
      @parent.try(:parent).try(:path_to_resource).to_a + [@resource, @current_id]
    end

    private

    def request(type, opts)
      fail BannedMethodError, "Method '#{type}' is not allowed for #{@resource}" if @banned_methods.include? type
      conn = ImageLoaderClient.api_manager.connection
      res = conn.send(type, path(opts.delete(:id)), opts)
      block_given? ? yield(res.status, res.body) : res
    rescue Faraday::ConnectionFailed => e
      raise ConnectionError, e.message
    end

    def path(resource_id)
      url_path = @parent.try(:path_to_resource).to_a
      url_path += [@resource, resource_id].compact
      url_path.unshift ImageLoaderClient::API_PREFIX
      url_path.map {|e| e.to_s.chomp('/').reverse.chomp('/').reverse }.join('/')
    end
  end
end
