require_relative 'requests'

module ImageLoaderClient
  class Api
    include Requests
    attr_reader :current_id, :parent

    def initialize(resource_name, options = {})
      @resource = resource_name
      @current_id = options[:current_id]
      @parent = options[:parent]
      @banned_methods = []
      filter_methods(options.slice(:only, :except))
      subresources(options[:subresources])
    end

    def filter_methods(options)
      return if options.blank?
      allowed_methods = %i(get post delete)
      @banned_methods = options[:except] ? (allowed_methods & options[:except]) :
        (allowed_methods - (options[:only] || []))
    end

    def subresources(hash)
      return if hash.blank?
      hash.each do |name, options|
        raise StandardError, "Method #{name} is mirrored" if respond_to? name
        options.merge!(parent: self)
        instance_variable_set(:"@subresource_#{name}", self.class.new(name, options))
        define_singleton_method name do
          raise ClientError, "Please provide and id for #{@resource} to fetch corresponding"\
          " #{name}, like this: ImageLoaderClient.#{@resource}(id).#{name}" unless @current_id
          instance_variable_get(:"@subresource_#{name}")
        end
      end
    end
  end
end
