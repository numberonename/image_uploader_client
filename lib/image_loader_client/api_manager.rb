module ImageLoaderClient
  class ApiManager
    ROOT_RESOURCES = %i(jobs)
    attr_reader :connection

    def initialize(url)
      @connection = Faraday::Connection.new(url: url) do |builder|
        builder.request  :json
        builder.response :json
        builder.adapter Faraday.default_adapter
      end
    end

    def jobs(id)
      Api.new(:jobs, {
                     current_id: id,
                     subresources: {
                       images: { only: [:get] }
                     }
                   })
    end
  end
end