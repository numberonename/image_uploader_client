describe ImageLoaderClient::Api do
  let(:subject) { ImageLoaderClient::Api }

  context 'filter_methods' do
    it 'filters out methods provided in :except array' do
      api = subject.new(:test, except: [:delete])
      expect(api.instance_variable_get('@banned_methods')).to eq [:delete]
    end

    it 'filters out methods not provided in :only array' do
      api = subject.new(:test, only: [:get])
      expect(api.instance_variable_get('@banned_methods').sort).to eq [:delete, :post].sort
    end
  end

  context 'subresources' do
    let(:bad_options) do
      { subresources: {
        filter_methods: { only: [:get] }
      }}
    end

    let(:bad_options_without_id) do
      { subresources: {
        images: { only: [:get] }
      }}
    end

    let(:good_options) do
      { current_id: 1, subresources: {
        images: { only: [:get] }
      }}
    end

    let(:subresource_name) { good_options[:subresources].keys.first }

    it 'fails if subresource name mirrors api method' do
      expect{ subject.new(:test, bad_options) }.to raise_error(StandardError)
    end

    it 'fails if no current id is given for subresource parent' do
      inst = subject.new(:test, bad_options_without_id)
      expect{ inst.send(subresource_name) }.to raise_error(ImageLoaderClient::ClientError)
    end

    it 'sets subresource as instance variable of the Api class with provided options' do
      inst = subject.new(:test, good_options)
      expect(inst.instance_variable_get("@subresource_#{subresource_name}")).to be_kind_of subject
    end

    it 'creates method that refers to instance variable of the subresource' do
      inst = subject.new(:test, good_options)
      inst_variable = inst.instance_variable_get("@subresource_#{subresource_name}")
      expect(inst_variable).to be
      expect(inst.send(subresource_name)).to eq inst_variable
    end

    it 'sets parent for subresource' do
      inst = subject.new(:test, good_options)
      subresource = inst.instance_variable_get("@subresource_#{subresource_name}")
      expect(subresource.parent).to eq inst
    end
  end
end
