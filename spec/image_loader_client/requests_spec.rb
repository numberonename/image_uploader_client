describe ImageLoaderClient::Requests do
  let(:subject) { ImageLoaderClient::Api }

  let(:api_options) do
    { current_id: 1, subresources: {
      images: { only: [:get] }
    }}
  end

  let(:subresource_name) { api_options[:subresources].keys.first }

  def resource_name_and_id(api)
    [api.instance_variable_get('@resource'), api.instance_variable_get('@current_id')]
  end

  it 'fails if banned method is called' do
    api = subject.new(:test, only: [:get])
    expect { api.post }.to raise_error(ImageLoaderClient::BannedMethodError)
  end

  context 'paths' do
    before :each do
      @api = subject.new(:jobs, api_options)
      @subresource = @api.send(subresource_name)
    end

    it 'gets correct path elements' do
      expect(@subresource.path_to_resource).to eq resource_name_and_id(@subresource)
    end

    it 'correctly displays path for resource' do
      expect(@subresource.send(:path, nil)).to eq("#{ImageLoaderClient::API_PREFIX}/"\
      "#{resource_name_and_id(@api).join('/')}/#{resource_name_and_id(@subresource).compact.join('/')}")
    end
  end
end
