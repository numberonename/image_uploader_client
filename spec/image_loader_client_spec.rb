describe ImageLoaderClient do
  let(:subject) { ImageLoaderClient }

  it 'defines proxy api_manager method' do
    expect(subject.api_manager).to be_kind_of ImageLoaderClient::ApiManager
    expect(subject.api_manager.object_id).to eq subject.api_manager.object_id
  end

  context 'proxy methods' do
    it 'defines proxy methods outlined by ApiManager' do
      ImageLoaderClient::ApiManager::ROOT_RESOURCES.each do |resource|
        expect(subject).to respond_to resource
      end
    end

    it 'calls ApiManager method if it is mirrored' do
      meth = ImageLoaderClient::ApiManager::ROOT_RESOURCES.sample
      expect(subject.send(meth).class).to eq subject.api_manager.send(meth, nil).class
    end
  end

  it 'lets change image loader url' do
    new_url = 'http://google.com'
    subject.image_loader_url = new_url
    expect(ImageLoaderClient.image_loader_url).to eq new_url
  end

  it "changes api_manager's Faraday connection to new url" do
    new_url = 'http://google.com'
    subject.image_loader_url = new_url
    expect(ImageLoaderClient.api_manager.connection.url_prefix.to_s).to match new_url
  end
end
