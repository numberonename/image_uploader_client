# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'image_loader_client/version'

Gem::Specification.new do |spec|
  spec.name          = 'image_loader_client'
  spec.version       = ImageLoaderClient::VERSION
  spec.authors       = ['Yaro Holoduke']
  spec.email         = ['pragmateme@gmail.com']

  spec.summary       = 'Client for image loader server'
  spec.description   = spec.summary
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.require_paths = ['lib']

  spec.add_dependency 'faraday'
  spec.add_dependency 'faraday_middleware'
  spec.add_dependency 'activesupport'

  spec.add_development_dependency 'rspec'
end
